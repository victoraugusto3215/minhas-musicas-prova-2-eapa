//
//  DetalheMusicaViewController.swift
//  Minhas Musicas - Prova 2 eapa
//
//  Created by COTEMIG on 18/08/22.
//

import UIKit

class DetalheMusicaViewController: UIViewController {

    var NomeImagem: String = ""
    var NomeMusica: String = ""
    var NomeAlbum: String = ""
    var NomeCantor: String = ""
    
    @IBOutlet weak var musica: UILabel!
    @IBOutlet weak var album: UILabel!
    @IBOutlet weak var capa: UIImageView!
    @IBOutlet weak var cantor: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.capa.image = UIImage(named: self.NomeImagem)
        self.musica.text = self.NomeMusica
        self.album.text = self.NomeAlbum
        self.cantor.text = self.NomeCantor
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
