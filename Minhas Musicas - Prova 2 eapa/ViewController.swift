//
//  ViewController.swift
//  Minhas Musicas - Prova 2 eapa
//
//  Created by COTEMIG on 18/08/22.
//

import UIKit

struct Musica {
    let nomeMusica: String
    let NomeCantor: String
    let Nomealbum: String
    let NomeImagemPequena: String
    let NomeImagemGrande: String
}

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var listademusica: [Musica] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listademusica.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableview.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let  musica = self.listademusica[indexPath.row]
        
        cell.musica.text = musica.nomeMusica
        cell.album.text = musica.Nomealbum
     
        
        
        
        cell.cantor.text = musica.NomeCantor
        cell.capa.image = UIImage(named: musica.NomeImagemPequena)
        
        return cell
    
        
    }
    
    
    
    @IBOutlet weak var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.dataSource = self
        self.tableview.delegate  = self
        
        self.listademusica.append(Musica(nomeMusica: "Pontos Cardeais", NomeCantor: "Alceu Valença", Nomealbum: "Álbum Vivo!", NomeImagemPequena: "capa_alceu_pequeno", NomeImagemGrande: "capa_alceu_grande"))
        
        self.listademusica.append(Musica(nomeMusica: "Menor Abandonado", NomeCantor: "Zeca Pagodinho", Nomealbum: "Álbum Patota de Cosme", NomeImagemPequena: "capa_zeca_pequeno", NomeImagemGrande: "capa_zeca_grande"))
        
        self.listademusica.append(Musica(nomeMusica: "Tiro ao Alvo", NomeCantor: "Adoniran Barbosa", Nomealbum: "Álbum Adoniran Barbosa e Convidados", NomeImagemPequena: "capa_adoniran_pequeno", NomeImagemGrande: "capa_adhoniran_grande"))
        
        
        // Do any additional setup after loading the view.
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.performSegue(withIdentifier: "abrirDetalhe", sender: indexPath.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        let DetalheViewcontroller = segue.destination as! DetalheMusicaViewController
        let indice = sender as! Int
        let musica = self.listademusica[indice]
        
        DetalheViewcontroller.NomeImagem = musica.NomeImagemGrande
        DetalheViewcontroller.NomeMusica = musica.nomeMusica
        DetalheViewcontroller.NomeCantor = musica.NomeCantor
        DetalheViewcontroller.NomeAlbum = musica.Nomealbum
    }
    
}


